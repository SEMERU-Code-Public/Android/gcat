from PIL import Image
from PIL import ImageChops
# import imagehash

class ImageSet:
    """Class that hashes images in order to eliminate duplicates."""
    def __init__(self):
        self.__images = dict()
        self.__size = 0

    def __len__(self):
        return self.__size

    def __contains__(self, img):
        #hsh = imagehash.phash(img)
        img = self._process_image(img)
        hsh = hash(img.tobytes())
        if hsh in self.__images:
            for image in self.__images[hsh]:
                if self.images_are_equal(image, img):
                    return True
        return False

    def images_are_equal(self, image1, image2):
        # This is used instead of the == operator, because == checks metainformation
        # in addition to graphical information, and we only care about graphical information.
        return ImageChops.difference(image1, image2).getbbox() is None

    def _process_image(self, image):
        """ Images need to be cropped to remove the notification bar."""
        width, height = image.size
        cropped_image = image.crop((0, 50, width, height)) # For ignored area
        return cropped_image


    def add(self, img):
        """ Attempts to add an image to the set. Returns true if successful,
            returns false on collision. """
        #hsh = imagehash.phash(img)
        img = self._process_image(img)
        hsh = hash(img.tobytes())
        
        if hsh in self.__images:
            for image in self.__images[hsh]:
                if self.images_are_equal(image, img):
                    return False
            self.__images[hsh].append(img)
        else:
            self.__images[hsh] = [img]
        
        self.__size += 1
        return True

    def get_images(self):
        imgs = []
        for key in self.__images:
            imgs += self.__images[key]
        return imgs

    def find_match(self, img):
        """ Attempts to find a matching image in the set. If none is found, return None."""
        img = self._process_image(img)
        hsh = hash(img.tobytes())

        if hsh in self.__images:
            for image in self.__images[hsh]:
                if self.images_are_equal(image, img):
                    return image
        return None
        
