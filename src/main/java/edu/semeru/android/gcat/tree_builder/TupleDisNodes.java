/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.tree_builder;

/**
 * Tuple containing the distance between an old commit node and a new commit 
 * node stored in that order. Used to match nodes before catching differences
 * between commits.
 * 
 * @author georgewpurnell
 */
public class TupleDisNodes implements Comparable<TupleDisNodes> {

    /**
     * The heuristic distance between two nodes
     */
    public float dis;
    
    /**
     * Old commit node
     */
    public UiTreeNode dsNode;

    /**
     * New commit node
     */
    public UiTreeNode uiNode;

    /**
     * Retrieve distance between two nodes.
     * 
     * @return the heuristic distance between two nodes
     */
    public float getDis() {
        return dis;
    }

    /**
     * Set the distance between two nodes.
     * 
     * @param dis the dis to set
     */
    public void setDis(float dis) {
        this.dis = dis;
    }

    /**
     * Retrieve old commit node.
     * 
     * @return the old commit node
     */
    public UiTreeNode getDsNode() {
        return dsNode;
    }

    /**
     * Set old commit node.
     * 
     * @param dsNode the old commit node to set
     */
    public void setDsNode(UiTreeNode dsNode) {
        this.dsNode = dsNode;
    }

    /**
     * Retrieve new commit node.
     * 
     * @return the new commit node
     */
    public UiTreeNode getUiNode() {
        return uiNode;
    }

    /**
     * Set new commit node.
     * 
     * @param uiNode the new commit node to set
     */
    public void setUiNode(UiTreeNode uiNode) {
        this.uiNode = uiNode;
    }


    /**
     * Constructor.
     * @param dis the distance between two nodes
     * @param dsNode the old commit node
     * @param uiNode the new commit node
     */
    public TupleDisNodes(float dis, UiTreeNode dsNode, UiTreeNode uiNode) {
        this.dis = dis;
        this.dsNode = dsNode;
        this.uiNode = uiNode;
    }

    @Override
    public String toString() {
        return "Weight: " + dis + ", " + dsNode.toString() + ",  ==> " + uiNode.toString();
    }

    @Override
    public int compareTo(final TupleDisNodes o) {
        return Float.compare(this.dis, o.dis);
    }

}
