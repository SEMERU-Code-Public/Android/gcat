/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.tree_builder;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.semeru.android.gcat.helpers.ImagesHelper;
import edu.semeru.android.gcat.matching.MatchingAnalysis;

/**
 * This class represents a Basic node in the tree
 *
 * @author KevinMoran, Carlos, Boyang
 */
public class BasicTreeNode {

    /**
     * new commit node -> old commit node or old commit node -> new commit node
     */
    public BasicTreeNode correspondingNode;

    /**
     * True if the matching is complete (grand)children are matching as well
     */
    public boolean completeMatching = false;
    private boolean hasViolation = false;

    /**
     * Parent of current node
     */
    protected BasicTreeNode mParent;

    /**
     * Children of current node
     */
    protected ArrayList<BasicTreeNode> mChildren = new ArrayList<BasicTreeNode>();

    /**
     * Array of attributes of current node
     */
    protected ArrayList<BasicTreeNode> attributes = new ArrayList<BasicTreeNode>();

    /**
     * x coordinate
     */
    protected int x,

    /**
     * y coordinate
     */
    y,

    /**
     * width
     */
    width,

    /**
     * height
     */
    height;

    /**
     * True if the current node is merged into a super node
     */
    protected boolean merged = false;

    /**
     * Whether the boundary fields are applicable for the node or not
     * RootWindowNode has no bounds, but UiNodes should
     */
    protected boolean mHasBounds = false;

    private float PIDDiffRatio;   //PID difference in percentage

    /**
     * Path to corresponding cropped image
     */
    protected String croppedImagePath;

    /**
     * String of html for separate tree page ("Difference-Tree.html")
     */
    protected String tree = "";

    /**
     * String of html for common tree page ("Full-Report.html")
     */
    protected String commonTree = "";

    /**
     * String of html for tree template ("treeTemplate.html")
     */
    protected String treeHtmlTemplate = "";

    /**
     * String of html for common tree template ("summaryTemplate.html")
     */
    protected String commonTreeHtmlTemplate = "";

    
    /*
     * String paths for templates and output files.
     */
    private String treeHtmlTemplatePath = "html" + File.separator + "res" +
            File.separator + "templates" + File.separator + "treeTemplate.html";
    private String commonTreeHtmlTemplatePath = "html" + File.separator + "res" +
            File.separator + "templates" + File.separator + "summaryTemplate.html";
    private String differenceTreePath = "html" + File.separator + "Difference-Tree.html";
    private String commonTreePath = "html" + File.separator + "Full-Report.html";
    

    /**
     * Container to store the first and second levels of prev and new for comparison
     */
    protected static ArrayList<String> baseContainer = new ArrayList<String>();
    protected static String storedCommonTree = "";
    
    /**
     * Constructor.
     */
    public BasicTreeNode() {
        try {
            treeHtmlTemplate = new Scanner(new File(treeHtmlTemplatePath)).useDelimiter("\\Z").next();
            commonTreeHtmlTemplate = new Scanner(new File(commonTreeHtmlTemplatePath)).useDelimiter("\\Z").next();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BasicTreeNode.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Constructor.
     * 
     * @param x x coordinate, top left corner
     * @param y y coordinate, top left corner
     * @param width width of component
     * @param height height of component
     */ 
    public BasicTreeNode(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        try {
            treeHtmlTemplate = new Scanner(new File(treeHtmlTemplatePath)).useDelimiter("\\Z").next();
            commonTreeHtmlTemplate = new Scanner(new File(commonTreeHtmlTemplatePath)).useDelimiter("\\Z").next();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BasicTreeNode.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Add child to current node
     * @param child node to add
     */
    public void addChild(BasicTreeNode child) {
        if (child == null) {
            throw new NullPointerException("Cannot add null child");
        }
        if (mChildren.contains(child)) {
            throw new IllegalArgumentException("node already a child");
        }
        mChildren.add(child);
        child.mParent = this;
    }

    /**
     * Retrieve list of children of current node
     * 
     * @return list of current node's children
     */
    public ArrayList<BasicTreeNode> getChildren() {
        return this.mChildren;
    }

    /**
     * Retrieve parent of current node
     * 
     * @return parent
     */
    public BasicTreeNode getParent() {
        return mParent;
    }

    /**
     * Remove all children
     */
    public void clearAllChildren() {
        for (BasicTreeNode child : mChildren) {
            child.clearAllChildren();
        }
        mChildren.clear();
    }

    /**
     * Retrieve corresponding node
     * 
     * @return the correspondingNode
     */
    public BasicTreeNode getCorrespondingNode() {
        return correspondingNode;
    }

    /**
     *
     * Find nodes in the tree containing the coordinate
     *
     * The found node should have bounds covering the coordinate, and none of
     * its children's bounds covers it. Depending on the layout, some app may
     * have multiple nodes matching it, the caller must provide a
     * {@link IFindNodeListener} to receive all found nodes
     *
     * @param px x coordinate
     * @param py y coordinate
     * @param listener listener for all found nodes
     * @return true if children found at point (px, py)
     */
    public boolean findLeafMostNodesAtPoint(int px, int py,
            IFindNodeListener listener) {
        boolean foundInChild = false;
        for (BasicTreeNode node : mChildren) {
            foundInChild |= node.findLeafMostNodesAtPoint(px, py, listener);
        }
        // checked all children, if at least one child covers the point, return
        // directly
        if (foundInChild) {
            return true;
        }
        // check self if the node has no children, or no child nodes covers the
        // point
        if (mHasBounds) {
            if (x <= px && px <= x + width && y <= py && py <= y + height) {
                listener.onFoundNode(this);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Retrieve attributes array
     * @return null
     */
    public Object[] getAttributesArray() {
        return null;
    }


    /**
     * Interface for findLeafMostNodesAtPoint listener
     */
    public static interface IFindNodeListener {

        /**
         * Listener
         * @param node found
         */
        void onFoundNode(BasicTreeNode node);
    }

    /**
     * Check if the current Node contains comparedNode
     *
     * @param comparedNode node to compare
     * @return true if current node contains comparedNode
     */
    public boolean contains(BasicTreeNode comparedNode) {
        return (comparedNode.x >= this.x) && (comparedNode.y >= this.y)
                && (comparedNode.x + comparedNode.width <= this.x + this.width)// + ConstantSettings.ADJUSTPIXELINDESIGN)
                && (comparedNode.y + comparedNode.height <= this.y + this.height);// + ConstantSettings.ADJUSTPIXELINDESIGN);
    }

    /**
     * Check if the current Node contains comparedNode with 0.2 threshold
     *
     * @param comparedNode node to compare
     * @return true if current node contains comparedNode within threshold
     */
    public boolean containsWithThreshold(BasicTreeNode comparedNode) {
        return (comparedNode.x >= this.x - this.width * 0.2) && (comparedNode.y >= this.y - this.height * 0.2)
                && (comparedNode.x + comparedNode.width <= this.x + this.width * 1.2)
                && (comparedNode.y + comparedNode.height <= this.y + this.height * 1.2);
    }

    /**
     * Check if the current Node intersects comparedNode
     * 
     * @param comparedNode node to compare
     * @return true if current node intersects comparedNode
     */
    public boolean intersect(BasicTreeNode comparedNode) {
        return !((comparedNode.x + comparedNode.width <= this.x)
                || (comparedNode.y + comparedNode.height <= this.y)
                || (comparedNode.x >= this.x + this.width)
                || (comparedNode.y >= this.y + this.height));
    }

    /**
     * Check if the current Node equals comparedNode
     * 
     * @param comparedNode node to compare
     * @return true if current node's fields equal comparedNode's fields (x, y, width, height)
     */
    public boolean equals(BasicTreeNode comparedNode) {
        return (comparedNode.x == this.x) && (comparedNode.y == this.y)
                && (comparedNode.width == this.width)
                && (comparedNode.height == this.height);
    }

    /**
     * Add node in to this object Node: this always contains node
     *
     * @param node node to add
     */
    public void addNode(BasicTreeNode node) {
        BasicTreeNode appendToChild = null;
        List<BasicTreeNode> found = new ArrayList<BasicTreeNode>();

        for (BasicTreeNode child : mChildren) {
            if (node.equals(child)) {
                return;
            } else if (node.contains(child)) {
                found.add(child);
                node.addNode(child);
            } else if (child.contains(node)) {
                //Important debug messages for detecting overlap exceptions. Do not delete.
                //				System.out.println("=================");
                //				System.out.println(appendToChild);
                //				System.out.println(child);
                //				System.out.println("=================");
                if (appendToChild != null) {
                    try {
                        throw new Exception();
                    } catch (Exception e) {
                    }
                }
                appendToChild = child;
            } else if (child.intersect(node)) {
            }
        }

        mChildren.removeAll(found);

        if (appendToChild == null) {
            mChildren.add(node);
            node.mParent = this;
        } else {
            appendToChild.addNode(node);
            //node should not have children
        }
    }

    /**
     * Delete child given specified position and size. Parameters given
     * in the form of an integer array [x, y, width, height]
     * 
     * @param posSize specified position and size to delete: [x, y, width, height]
     */
    public void deleteChildByPosSize(int[] posSize) {
        for (BasicTreeNode child : this.mChildren) {
            if (child.x == posSize[0] && child.y == posSize[1] && child.width == posSize[2] && child.height == posSize[3]) {
                mChildren.remove(child);
                return;
            }
        }

    }


    /**
     * Depth first tree with HTML tags. Used to generate html output
     * 
     * @param type "common", "prev" or "new" types of tree
     */
    public void printOutHTMLTree(String type) {
        int level = -1;
        //tree = "";
        for (BasicTreeNode node : this.mChildren) {
            if(type != "common"){
                tree = printOutHTMLTreeHelper(level + 1, node, tree);
            }
            else{
                commonTree = printOutHTMLTreeHelper(level + 1, node, commonTree);
            }
        }
        PrintWriter writer = null;
        
        // Generate old commit tree
        if (type == "prev") {
            try {               
                writer = new PrintWriter(differenceTreePath, "UTF-8");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(BasicTreeNode.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(BasicTreeNode.class.getName()).log(Level.SEVERE, null, ex);
            }
            tree = treeHtmlTemplate.replace("$tree", "<div id=\"prevTree\">Previous Tree</div>" + tree + "<div class=\"buffer\"></div></ul><ul class=\"collapsibleList\">$tree</ul>");
            writer.println(tree);
            writer.close();
        }
        // Generate new commit tree
        else if(type == "new"){       
            try {
                treeHtmlTemplate = new Scanner(new File(differenceTreePath)).useDelimiter("\\Z").next();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(BasicTreeNode.class.getName()).log(Level.SEVERE, null, ex);
            }
            tree = treeHtmlTemplate.replace("$tree", "<div id=\"newTree\">New Tree</div>" + tree + "<div class=\"buffer\"></div>");

            try {               
                writer = new PrintWriter(differenceTreePath, "UTF-8");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(BasicTreeNode.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(BasicTreeNode.class.getName()).log(Level.SEVERE, null, ex);
            }
            writer.println(tree);
            writer.close();
        }
        
        // Generate maximum common tree
        else {//if(type == "common" && writer != null){ 
            try {  
                writer = new PrintWriter(commonTreePath, "UTF-8");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(BasicTreeNode.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(BasicTreeNode.class.getName()).log(Level.SEVERE, null, ex);
            }
            commonTree = "<div id=\"commonTree\">&zwnj;</div>" + commonTree + "<div class=\"buffer\"></div>";
            setStoredCommonTree(commonTree);
            commonTree = commonTreeHtmlTemplate.replace("$tree", commonTree);
            writer.close();
        }
    }

    /**
     * A helper for generating html trees
     * @param level current level
     * @param node current root node
     * @param tree string of current html for tree
     * @return appended html for tree
     */
    public String printOutHTMLTreeHelper(int level, BasicTreeNode node, String tree) {
        String branch = "";
        if (level < 2 && baseContainer.size() < 6){
            branch += "<li style=\"color:white;\" class=\"m-wrap\"><span class= " + String.valueOf(level) +  "> Level " + level + ": ";
        }
        else if(node.hasViolation){ 
            branch += "<li style=\"color:red;\" class=\"m-wrap\"> Level " + level + ": ";
        }
        else{
            branch += "<li style=\"color:white;\" class=\"m-wrap\"> Level " + level + ": ";
        }
        branch += "Start Pos: " + node.x + "," + node.y;
        if (node instanceof UiTreeNode) {
            
            UiTreeNode un = (UiTreeNode) node;
            branch += " Type(class): " + un.getType();
            branch += " ID: " + un.getId();
            branch += " Name: " + un.getName();
            branch += " Type(class): " + un.getType();
            branch += " Rectangle: " + node.width + "," + node.height;
            branch += un.getElementString();
        }
        branch += "\n";
        if (level < 2){
            branch += "</span>";
            if (baseContainer.size() == 4){
                if (!baseContainer.get(0).equals(baseContainer.get(2))){
                    level--;
                    addToBaseContainer(branch);
                    branch = "";
                }
            }
            else if (baseContainer.size() == 5){
                if (!baseContainer.get(1).equals(baseContainer.get(3))){
                    level--;
                    addToBaseContainer(branch);
                    branch =  "<li style=\"color:white;\" class=\"m-wrap\"><span class= 0 >No Common Root \n</span>";//<li style=\"color:white;\"> No Common Root \n";
                }
            }
            else {
                addToBaseContainer(branch);
            }
        }
        tree += branch;
        for (BasicTreeNode child : node.getChildren()) {
            tree += "<ul>";
            tree = printOutHTMLTreeHelper(level + 1, child,tree);
            tree += "</ul>";
        }
        tree += "</li>";
        return tree;
    }
    
    
    /**
	 * Depth first print out of tree
	 */
	public void printOutTree(){
		System.out.print("|");
		int level = -1;
		for(BasicTreeNode node : this.mChildren){
			printOutTreeHelper(level+1, node);
		}
	}

    /**
     * A helper for printing out trees
     * 
     * @param level current level
     * @param node current root node
     */
    public void printOutTreeHelper(int level, BasicTreeNode node){
		System.out.print("|Level " + level+ "|");
		for(int i = 0; i < level ; i ++){
			System.out.print("--------");
		}
		System.out.print("Start Pos:" + node.x + "," + node.y);
		System.out.print("\trectangle:" + node.width + "," + node.height);

		if(node instanceof UiTreeNode){
			UiTreeNode un = (UiTreeNode)node;
//			System.out.print("\tID:" + un.getId());
//			System.out.print("\t name:" + un.getName());
//			System.out.print("\t type(class):" + un.getType());
//			System.out.println(un.getElementString());
		}
		System.out.print("\n");
		for(BasicTreeNode child : node.getChildren()){
			printOutTreeHelper(level+1, child);
		}
	}

    private HashSet<BasicTreeNode> bnSet = new HashSet<BasicTreeNode>();

    /**
     * Return all sub-nodes under this root.
     *
     * @return all sub-nodes under root
     */
    public HashSet<BasicTreeNode> getAllNodes() {
        bnSet = new HashSet<BasicTreeNode>();
        for (BasicTreeNode child : this.getChildren()) {
            getAllNodesHelper(child);
        }
        return this.bnSet;
    }

    /**
     * A helper function for returning all sub-nodes under
     *
     * @param node
     */
    public void getAllNodesHelper(BasicTreeNode node) {

        this.bnSet.add(node);
        for (BasicTreeNode child : node.getChildren()) {
            getAllNodesHelper(child);
        }
    }

    /**
     * Return all getAllLeafNodes nodes under this.
     *
     * @return
     */
    public HashSet<BasicTreeNode> getAllIncompletedNodes() {
        bnSet = new HashSet<BasicTreeNode>();
        for (BasicTreeNode child : this.getChildren()) {
            getAllIncompletedNodesHelper(child);
        }
        return this.bnSet;
    }

    /**
     * A helper function for returning all getAllLeafNodes under
     *
     * @param node
     */
    public void getAllIncompletedNodesHelper(BasicTreeNode node) {
        if (node.completeMatching == false) {
            this.bnSet.add(node);
        } else {
            for (BasicTreeNode child : node.getChildren()) {
                getAllIncompletedNodesHelper(child);
            }
        }
    }

    /**
     * Retrieve cropped image path.
     * 
     * @param type "new" or "old" specifies if node is from new or old commit
     * @return cropped image path
     */
    public String getCroppedImg(String type) {

        if (this instanceof UiTreeNode) {  
            String fileStr = MatchingAnalysis.outputFolder + "implement" + File.separator + this.hashCode() + ".jpg";
            if ("new".equals(type)){
                fileStr = MatchingAnalysis.outputFolder + "implement" + File.separator + this.hashCode() + ".jpg";
            }else if ("old".equals(type)){
                fileStr = MatchingAnalysis.outputFolder + "design" + File.separator + this.hashCode() + ".jpg";
            }
            File file = new File(fileStr);
            if (file.exists()) {
                return fileStr;
            } else {
                return "";
            }
        }

        return null;
    }

    /**
     * Retrieve top three colors found in node location
     * 
     * @param type "new" or "old" specifies if node is from new or old commit
     * @return top three colors 
     */
    public String[] getTop3Colors(String type) {
        String imgLocation = this.getCroppedImg(type);
        String[] returnHex = new String[3];
        try {
            Color[] colors = ImagesHelper.quantizeImageAndGetColors(imgLocation, 3);
            int pos = 0;
            for (Color color : colors) {
                if (color != null) {
                    String hex = "#" + Integer.toHexString(color.getRGB()).substring(2);
                    returnHex[pos++] = hex;
                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return returnHex;
    }


    /**
     * Get the top line (vertical value)
     *
     * @return x coordinate
     */
    public int getX() {
        return x;
    }

    /**
     * Get the left line (horizontal value)
     *
     * @return y coordinate
     */
    public int getY() {
        return y;
    }

    /**
     * Get the right line (horizontal value)
     *
     * @return width
     */
    public int getWidth() {
        return width;
    }
    
    /**
     * Get the bottom line (vertical value)
     *
     * @return height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Retrieve current node's children
     * 
     * @return array of current node's children
     */
    public ArrayList<BasicTreeNode> getmChildren() {
        return mChildren;
    }

    /**
     * Retrieve current node's parent
     * 
     * @return current node's parent
     */
    public BasicTreeNode getmParent() {
        return mParent;
    }

    /**
     * Retrieve PID difference ratio
     * 
     * @return PID difference ratio
     */
    public float getPIDDiffRatio() {
        return PIDDiffRatio;
    }

    
    /**
     * Set PID difference ratio
     * @param pIDDiffRatio PID difference ratio to set
     */
    public void setPIDDiffRatio(float pIDDiffRatio) {
        PIDDiffRatio = pIDDiffRatio;
    }

    /**
     * Retrieve merge status
     * 
     * @return merge status
     */
    public boolean isMerged() {
        return merged;
    }

    /**
     * Set merge status
     * 
     * @param merged merge status to set
     */
    public void setMerged(boolean merged) {
        this.merged = merged;
    }

    
    /**
     * Retrieve cropped image path
     * @return cropped image path
     */
    public String getCroppedImagePath() {
        return croppedImagePath;
    }
    
    
    /**
     * Set cropped image path
     * @param imagePath image path to set
     */
    public void setCroppedImagePath(String imagePath) {
        croppedImagePath = imagePath;
    }

    /**
     * Retrieve stored common tree
     * @return common tree
     */
    public static String getStoredCommonTree(){
        return storedCommonTree;
    }
    
    
    /**
     * Set stored common tree
     * 
     * @param tree common tree to set
     */
    public void setStoredCommonTree(String tree){
        storedCommonTree = tree;
    }
   

    /**
     * Set violation flag to true
     */
    public void setHasViolation(){
        hasViolation = true;
    }
    
    
    private void addToBaseContainer(String level){
        baseContainer.add(level);
    }
}
