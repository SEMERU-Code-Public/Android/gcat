/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.screen_matching;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

import edu.semeru.android.gcat.helpers.DataHelper.Version;

/**
 *
 * @author jhoskins
 */
public class CSOutputTransform {
    
    public static void transform(String dataRoot, String outputFolder) {
        //String newFolders = "/home/jhoskins/newFolders";
        
        Map<String, ArrayList<Version>> allData = getPackageNames(dataRoot);
        try {
            System.out.println("Merging...");
            mergeFolders(dataRoot, outputFolder, allData);
            System.out.println("Stepping out xml...");
            stepOutXml(outputFolder);
            System.out.println("Deleting images...");
            deleteSuperfluousImages(outputFolder);
            System.out.println("Splitting");
            splitTopDownBottomUp(outputFolder);
        } catch (Exception ex) {
            Logger.getLogger(CSOutputTransform.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private static HashMap<String, ArrayList<Version>> getPackageNames(String dataRoot) {
        File file = new File(dataRoot);
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
        
        HashMap<String, ArrayList<Version>> packageNames = new HashMap();
        for (String s : directories) {
            String packageName = s.split("-")[0];
            String version = s.split("-")[1];
            if (packageNames.containsKey(packageName)) {
                packageNames.get(packageName).add(new Version(version));
            } else {
                ArrayList<Version> arr = new ArrayList();
                arr.add(new Version(version));
                packageNames.put(packageName, arr);
            }
        }
        
        for (Map.Entry<String, ArrayList<Version>> entry : packageNames.entrySet()) {
            Collections.sort(entry.getValue());
        }
        
        return packageNames;
    }
    
    private static void mergeFolders(String dataRoot, String newFolders, Map<String, ArrayList<Version>> allData) {
    for (Map.Entry<String, ArrayList<Version>> entry : allData.entrySet()) {
        String packageName = entry.getKey();
        ArrayList<Version> versions = entry.getValue();
        for (int i = 0; i < versions.size() - 1; i++) {
            Version v1 = versions.get(i);
            Version v2 = versions.get(i + 1);
            File newFolder = new File(newFolders + File.separator + packageName + "-" + v1.get() + "-vs-" + v2.get());
            newFolder.mkdir();
            try {
                FileUtils.copyDirectory(new File(dataRoot + File.separator + packageName + "-" + v1.get()), newFolder);
                FileUtils.copyDirectory(new File(dataRoot + File.separator + packageName + "-" + v2.get()), newFolder);
            } catch (IOException ex) {
                Logger.getLogger(CSOutputTransform.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
            
        entry.getValue();
        }
    }
    
    public static void stepOutXml(String dataRoot) {
        File file = new File(dataRoot);
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
        
        for (String s : directories) {
            System.out.println(s);
            String absPath = dataRoot + File.separator + s;
            File src = new File(absPath + File.separator + "screenshots");
            File[] xmlFiles = src.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".xml");
                }
            });
            for (File f : xmlFiles) {
                System.out.println(f.getAbsolutePath());
                f.renameTo(new File(absPath + File.separator + f.getName()));
            }
        }
    }
    
    public static void deleteSuperfluousImages(String dataRoot) {
         File file = new File(dataRoot);
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
        
        for (String s : directories) {
            System.out.println(s);
            String absPath = dataRoot + File.separator + s;
            File src = new File(absPath + File.separator + "screenshots");
            File[] badFiles = src.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith("_augmented.png") || name.toLowerCase().endsWith("_gui.png");
                }
            });
            for (File f : badFiles) {
                f.delete();
            }
        }
    }
    
    public static void splitTopDownBottomUp(String dataRoot) throws Exception {
        File file = new File(dataRoot);
        File[] dirs = file.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return new File(dir, name).isDirectory();
            }
        });
        for (File f : dirs) {
            String name = f.getAbsolutePath();
            try {
                FileUtils.copyDirectory(f, new File(name + "-Bottom_Up"));
            } catch (IOException ex) {
                Logger.getLogger(CSOutputTransform.class.getName()).log(Level.SEVERE, null, ex);
            }
            f.renameTo(new File(name + "-Top_Down"));
        }
        
        
        File[] dirs2 = file.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return new File(dir, name).isDirectory();
            }
        });
        
        for (File f : dirs2) {
            String[] split = f.getName().split("-");
            String type = split[split.length - 1];
            
            if (type.equals("Top_Down")) {
                deleteAllWith(f.getAbsoluteFile(), "Bottom_Up");
                deleteAllWith(new File(f.getAbsolutePath() + File.separator + "screenshots"), "Bottom_Up");
                System.out.println(type);
            } else  if (type.equals("Bottom_Up")) {
                deleteAllWith(f.getAbsoluteFile(), "Top_Down");
                deleteAllWith(new File(f.getAbsolutePath() + File.separator + "screenshots"), "Top_Down");
                System.out.println(type);
            } else {
                throw new Exception("Illegal type " + type);
            }
        }
    }
    
    public static void deleteAllWith(File dir, String string) {
        File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.contains(string);
            }
        });
        
        for (File f : files) {
            f.delete();
        }
    }
    
    public static void main(String args[]) {
        transform("/home/jhoskins/Downloads/testFINAL", "/home/jhoskins/newFolders");
    }
    
}
