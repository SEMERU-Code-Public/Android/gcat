/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.nlgeneration;

import static edu.semeru.android.gcat.nlgeneration.LanguageConverter.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.semeru.android.gcat.changes.Change;
import edu.semeru.android.gcat.changes.ChangeType;
import edu.semeru.android.gcat.helpers.PIDHelper;
import edu.semeru.android.gcat.matching.MatchingAnalysis;
import edu.semeru.android.gcat.tree_builder.ConstantSettings;

/**
 *Class that generates the natural language summary for the overall changes to the GUI
 * @author cdest
 */
public class SummaryGenerator {
    
   private String summary;
   private List<Change> violations;
   private HashMap<ChangeType, Integer> violationCounter;

    /**
     * Constructor
     * @param violations list of violations
     */
    public SummaryGenerator(List<Change> violations){
        summary = "";     
        this.violations = violations;
    }
    /*
    Where the natural language log is created, and the violation counts are 
    parsed and interpreted.
    */

    /**
     * Generates natural language summary
     * @param violationCounter 
     * @return
     */

    public String generateSummary(HashMap violationCounter){
//        System.out.println("GENERATING");
        this.violationCounter = violationCounter;
        String level = imageDifferenceChecker();
        String loc = locationFinder();
        String amount = amountOfChange();
        System.out.println();
        System.out.println("Summary:");
        System.out.println("  " + makeCompleteSummary(level, loc, amount, violationCounter)); // Prints indented on multi lines
//        System.out.println("Level: " + level);
//        System.out.println("Location: " + loc);
//        System.out.println("Amount change:" + amount);
        
        return makeCompleteSummary(level, loc, amount, violationCounter);
    }
    
    
    private boolean differenceHeuristicIsTrue(double areaOne, double areaTwo) {
        //return areaOne > (2 * areaTwo);
        return areaTwo == 0;
    }
    
    /**
     * Calculate density of change
     * @param numberOfRows
     * @param numberOfCols
     * @return 
     */
    public int[] getDensityIndicies(int numberOfRows, int numberOfCols) {
        RegionBoard regionBoard = new RegionBoard(numberOfRows, numberOfCols);
        for (Change v : violations)
            regionBoard.addViolation(v);
        
        int row = -1;
        int col = -1;
        
        ArrayList<Region> rows = regionBoard.getRowRegions();
        Collections.sort(rows);
        double rowDifference = rows.get(0).getArea() - rows.get(1).getArea();
        if (differenceHeuristicIsTrue(rowDifference, rows.get(1).getArea())) // Arbitrary heuristic
            row = rows.get(0).getIndex();
            
        ArrayList<Region> cols = regionBoard.getColumnRegions();
        Collections.sort(cols);
        double colDifference = cols.get(0).getArea() - cols.get(1).getArea();
        if (differenceHeuristicIsTrue(colDifference, cols.get(1).getArea())) // Arbitrary heuristic
            col = cols.get(0).getIndex();
        
//        System.out.println("for a " + numberOfRows +"x"+ numberOfCols + " division, row: " + row + " col: " + col);
        return new int[] {row, col};
    }
    
    //Todo: find the location on the screen that experienced the most changes
    /**
     * Find location on the screen that experienced changes
     * @return location of changes
     */
    private String locationFinder() {
        
        int[] density3x3 = getDensityIndicies(3, 3);
        int row = density3x3[0];
        int col = density3x3[1];
        
        if (row == -1 && col == -1) {
            int[] density2x2 = getDensityIndicies(2, 2);
            row = density2x2[0];
            col = density2x2[1];
            return convertIndicesToString2x2(row, col);
        } else 
            return convertIndicesToString3x3(row, col);
    }

    /**
     * Calculate change heuristic
     * DEPRECATED: USE imageDifferenceChecker() INSTEAD.
     * @return level of change
     */
    private String changeLevel() {
        
        String level = "";
        //might have an issue with not returning the total number of nodes we want
        if (this.violations.size() == 0)
            return "";
        
        if (violationCounter.get(ChangeType.Extra_Component) > 0 || 
        violationCounter.get(ChangeType.MISSING_COMPONENT) > 0) {
            level = "major";
        } else if (violationCounter.get(ChangeType.Image_DIFF) > 0) {
            level = imageDifferenceChecker();
        } else if (violationCounter.get(ChangeType.LOCATION_CHANGE) > 0) {
            level = imageDifferenceChecker();
        } else if (violationCounter.get(ChangeType.COLOR_MISMATCH) > 0 || 
                violationCounter.get(ChangeType.Font_MISMATCH) > 0) {
            level = "moderate";
        } else {
            level = "minor";
        }
        return level;
    }
    
    
    /*
     * Generate a string describing the level of pixel difference between the screens.
     */
    private String imageDifferenceChecker(){
        String level = "";
        double diff = 0.0;
        
        try {   
            diff = PIDHelper.getPixelDifference(MatchingAnalysis.PIDImagePath, 0, 0, ConstantSettings.getInstance().getUIBoard()[3], ConstantSettings.getInstance().getUIBoard()[2]);
        } catch (IOException ex) {
            Logger.getLogger(SummaryGenerator.class.getName()).log(Level.SEVERE, null, ex);
        } 
       
        if (diff < 5)
            level= "mild";
        else if (diff < 30)
            level = "moderate";
        else
            level = "major";
        return level;
    }
    
    
    /*
     * Generate amount of changes in natural language
     */
    private String amountOfChange(){
        String amount = "";
        int countViolations = this.violations.size();
        if (countViolations == 0)
            amount = "";
        if (countViolations < 2)
            amount = "not many changes";
        else if (countViolations < 5)
            amount = "a few changes";
        else
            amount = "many changes";
        return amount;
    }
    
    /**
     * Retrieve summary
     * @return natural language summary
     */
    public String getSummary() {
        return this.summary;
    }

}
