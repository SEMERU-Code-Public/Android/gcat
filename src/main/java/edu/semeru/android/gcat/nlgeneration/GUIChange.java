/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.nlgeneration;

import java.io.File;

/** 
 * Container that represents a singular change between two GUIS, optimized for 
 * representation in our HTML report.
 * @author cdest
 */
public class GUIChange {
    private String naturalLanguageDescription = ""; // A string that explains the change in natural language
    private String oldCroppedImagePath = ""; // Path to an image of the old (pre-change) component
    private String newCroppedImagePath = ""; // Path to an image of the new (post-change) component
    private static String htmlTag = "html" + File.separator; // Used to ensure file paths point to the correct place.
    
    /**
     * Constructor
     * @param description natural language description of change
     * @param oldCroppedImagePath old cropped image path
     * @param newCroppedImagePath new cropped image path
     */
    public GUIChange(String description, String oldCroppedImagePath, String newCroppedImagePath){
        if (oldCroppedImagePath != null) {
            // Remove extraneous html tag
            if (oldCroppedImagePath.contains(htmlTag)) {
                oldCroppedImagePath = oldCroppedImagePath.replace(htmlTag, "");
            }
        }
        if (newCroppedImagePath != null) {
            // Remove extraneous html tag
            if (newCroppedImagePath.contains(htmlTag)) {
                newCroppedImagePath = newCroppedImagePath.replace(htmlTag, "");
            }
        }
        this.naturalLanguageDescription = description;
        this.oldCroppedImagePath = oldCroppedImagePath;
        this.newCroppedImagePath = newCroppedImagePath;
    }
    
    /**
     * Get the natural language description associated with this particular change.
     * @return Natural language description of the change.
     */
    public String getNaturalLanguageDescription() {
        return this.naturalLanguageDescription;
    }

    /**
     * Get the old, pre-change component image path associated with this particular change.
     * @return Image path to pre-change component.
     */    
    public String getOldCroppedImagePath() {
        return this.oldCroppedImagePath;
    }
    
    /**
     * Get the new, post-change component image path associated with this particular change.
     * @return Image path to post-change component.
     */
    public String getNewCroppedImagePath() {
        return this.newCroppedImagePath;
    }
}
