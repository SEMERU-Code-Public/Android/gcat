/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.changes;

import edu.semeru.android.gcat.tree_builder.UiTreeNode;

/**
 * Violation that describes the case when the font type of a component is changed. 
 *
 */
public class FontChange extends Change {

    /**
     * The missing node in design
     */
    UiTreeNode fontMismatchNode = null;

    float diffRatio = 0;

    /**
     * Constructor.
     * 
     * @param imageDiffNode old commit node with different font
     * @param uiNode new commit node with different font
     * @param diffRatio difference ratio from PID analysis
     */
    public FontChange(UiTreeNode imageDiffNode, UiTreeNode uiNode, float diffRatio) {
        super(ChangeType.Font_MISMATCH);
        super.newNode = uiNode;
        this.fontMismatchNode = imageDiffNode;
        this.diffRatio = diffRatio;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("The "+ newNode.getType() + " at location " + "(" + newNode.getX() + "," + newNode.getY() + ") has changed font.");
        //sb.append(diffRatio + "% of the pixels in the implementation are different from the design.");
        return sb.toString();
    }

    /**
     *
     * @return true
     */
    @Override
    public boolean isExceededThreshold() {
        return true;
    }

    /**
     * Retrieve node with font mismatch
     * @return old commit node with different font
     */
    public UiTreeNode getFontMismatchNode(){
		return fontMismatchNode;
	}
}
