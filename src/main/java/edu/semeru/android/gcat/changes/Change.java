/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package edu.semeru.android.gcat.changes;

import edu.semeru.android.gcat.tree_builder.UiTreeNode;

/**
 * Encapsulates the a change between two GUIs.
 * @author Boyang Li Created on Nov 07, 2016
 */
@SuppressWarnings("rawtypes")
public abstract class Change implements Comparable {

    /**
     *
     */
    public ChangeType type;

    /**
     *
     */
    protected UiTreeNode newNode; // post-change node

    /**
     *
     */
    protected UiTreeNode oldNode; // pre-change node

    /**
     *
     * @return
     */
    public abstract boolean isExceededThreshold();

    /**
     * Constructor.
     * 
     * @param type type of violation
     */
    public Change(ChangeType type) {
        this.type = type;
    }

    /**
     * Get the new GUI version's newNode that's associated with the violation. 
     *
     * @return node from new commit
     */
    public UiTreeNode getNewNode() {
        return newNode;
    }
    
    /**
     * Get the old GUI version's newNode that's associated with the violation. 
     *
     * @return node from old commit
     */
    public UiTreeNode getOldNode() {
        return (UiTreeNode) newNode.correspondingNode;
    }

    /**
     * Get image difference ratio by PID analysis.
     *
     * @return difference ratio from PID analysis
     */
    public float getPIDDiffRatio() {
        UiTreeNode prev = (UiTreeNode) newNode.correspondingNode;
        if (prev != null) {
            return prev.getPIDDiffRatio();
        } else {
            return newNode.getPIDDiffRatio();
            //TODO: change value
            //return 100;
        }
    }

    /**
     * Get a string representing the area affected by the violation.
     * @return string representing the area affected by a violation
     */
    public String getUiInfo() {
        return newNode.getName() + "[" + newNode.getX() + ", " + newNode.getY() + "][" + newNode.getWidth() + ", " + newNode.getHeight() + "] ";
    }

    @Override
    public int compareTo(Object other) {
        if (!(other instanceof Change)) {
            throw new ClassCastException("A Violation object expected.");
        }
        return this.type.compareTo(((Change) other).type);
    }
}
