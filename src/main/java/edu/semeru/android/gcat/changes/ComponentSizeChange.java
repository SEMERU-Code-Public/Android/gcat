/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.changes;

import edu.semeru.android.gcat.tree_builder.ConstantSettings;
import edu.semeru.android.gcat.tree_builder.UiTreeNode;

/**
 * Violation that describes the case when a GUI component changes in size.
 * @author Boyang Li Created on Nov 07, 2016
 */
public class ComponentSizeChange extends Change {

    private int oldSize;
    private int newSize;
    private boolean isVertical;

    /**
     * Constructor.
     * 
     * @param uiNode node from new commit with different size
     * @param isVertical true if different widths
     * @param designSize size of node from old commit
     * @param realSize size of node from new commit
     */
    public ComponentSizeChange(UiTreeNode uiNode, boolean isVertical, int designSize, int realSize) {
        super(ChangeType.COMPONENT_SIZE);
        super.newNode = uiNode;
        this.isVertical = isVertical;
        this.oldSize = designSize;
        this.newSize = realSize;
    }

    /**
     *
     * @return true if size difference is greater than violation threshold
     */
    @Override
    public boolean isExceededThreshold() {
        ConstantSettings settings = ConstantSettings.getInstance();
        return Math.abs(oldSize - newSize) > settings.getViolationThreshold();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("The "+ newNode.getType() + " at location " + "(" + newNode.getX() + "," + newNode.getY() + ") has changed size");
        if (isVertical) {
            sb.append(" vertically from ");
        } else {
            sb.append(" horizontally from ");
        }
        sb.append(oldSize + " px to " + newSize + " px");
        return sb.toString();
    }

}
