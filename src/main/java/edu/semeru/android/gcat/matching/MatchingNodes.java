/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.matching;

import java.util.ArrayList;
import java.util.Collections;

import edu.semeru.android.gcat.helpers.RelativePositionConverter;
import edu.semeru.android.gcat.tree_builder.ConstantSettings;
import edu.semeru.android.gcat.tree_builder.RootUINode;
import edu.semeru.android.gcat.tree_builder.TupleDisNodes;
import edu.semeru.android.gcat.tree_builder.UiTreeNode;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Adapted from Boyang Li's TreeNodesMatching.java. Matches leaf nodes of
 * previous and recent commit trees.
 *
 * @author Louisa
 */
public class MatchingNodes {

    RootUINode rootD; // Previous commit tree
    RootUINode rootU; // Current commit tree

    /**
     * Constructor
     *
     * @param rootD previous commit tree
     * @param rootU new commit tree
     */
    public MatchingNodes(RootUINode rootD, RootUINode rootU) {
        this.rootD = rootD;
        this.rootU = rootU;
    }

    /**
     * Match leaf nodes in both trees.
     */
    public void matching() {
        ConstantSettings settings = ConstantSettings.getInstance();
        ArrayList<UiTreeNode> dsLeafNodes = rootD.getLeafNodes();
        ArrayList<UiTreeNode> uiLeafNodes = rootU.getLeafNodes();

        // Remove Layout nodes from both arrays. Previous commit tree.
        for (int i = 0; i < dsLeafNodes.size(); i++) {
            if (dsLeafNodes.get(i).getType().contains("Layout")) {
//				System.out.println(uiLeafNodes.get(i).getType());
                dsLeafNodes.remove(i);
            }
        }
        // Current commit tree.
        for (int i = 0; i < uiLeafNodes.size(); i++) {
            if (uiLeafNodes.get(i).getType().contains("Layout")) {
//				System.out.println(uiLeafNodes.get(i).getType());
                uiLeafNodes.remove(i);
            }
        }

        RelativePositionConverter converter = new RelativePositionConverter(settings.getDSBoard(), settings.getUIBoard());

        ArrayList<TupleDisNodes> listTuple = new ArrayList<TupleDisNodes>();
        // Contains both nodes with distance difference between them.
        for (UiTreeNode dsNode : dsLeafNodes) {
            for (UiTreeNode uiNode : uiLeafNodes) {
                if (uiNode.getWidth() < 3 || uiNode.getHeight() < 3) {
                    continue;
                }
                float curDis = converter.heuristicDistance(dsNode, uiNode);

                listTuple.add(new TupleDisNodes(curDis, dsNode, uiNode));
            }
        }
        Collections.sort(listTuple);

        // Match leaf nodes if distance difference less than threshold distance.
        for (TupleDisNodes tuple : listTuple) {
            UiTreeNode dsNode = tuple.getDsNode();
            UiTreeNode uiNode = tuple.getUiNode();
            if (tuple.getDis() < settings.getThresholdMatchDistance()) {
                // Leaf nodes match
                if (dsNode.correspondingNode == null && uiNode.correspondingNode == null) {
                    dsNode.correspondingNode = uiNode;
                    uiNode.correspondingNode = dsNode;
                    /* Print out to display corresponding nodes attributes
                    System.out.println("Coresponding node " + dsNode.correspondingNode + " " + uiNode.correspondingNode);
                    System.out.println(dsNode.correspondingNode.getX() + " Corresponds to " + uiNode.correspondingNode.getX());
                    System.out.println(dsNode.correspondingNode.getY() + " Corresponds to " + uiNode.correspondingNode.getY());
                    System.out.println(dsNode.correspondingNode.getWidth() + " Corresponds to " + uiNode.correspondingNode.getWidth());
                    System.out.println(dsNode.correspondingNode.getHeight() + " Corresponds to " + uiNode.correspondingNode.getHeight());
                    System.out.println("  ");
                    */
                }
            } else { // Nodes do not match
                break;
            }
        }
    }
}
