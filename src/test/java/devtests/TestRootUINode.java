/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package devtests;

import org.junit.Assert;
import org.junit.Test;

import edu.semeru.android.gcat.tree_builder.RootUINode;
import edu.semeru.android.gcat.tree_builder.UIDumpParser;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Make two empty rootUINodes. They should be equal
 *
 * @author georgewpurnell
 */
public class TestRootUINode {

    /**
     *
     */
    @Test
    public void TestRootUINodeEquality1() {
        RootUINode node1 = new RootUINode();
        RootUINode node2 = new RootUINode();
        Assert.assertTrue(node1.equals(node2));
    }

    /**
     * Make one empty rootUINode and one node with a name and rotation. They
     * should not be equal.
     *
     * @author georgewpurnell
     */
    @Test
    public void TestRootUINodeEquality2() { // note for these tests to work need to expand equals definition
        RootUINode node1 = new RootUINode();
        RootUINode node2 = new RootUINode("Window Name", "Rotation");
        Assert.assertFalse(node1.equals(node2));
    }

    /**
     * Make two RootUINodes with the same window name and same rotation. They
     * should be equal
     *
     * @author georgewpurnell
     */
    @Test
    public void TestRootUINodeEquality3() {
        RootUINode node1 = new RootUINode("Same Window Name", "Same Rotation");
        RootUINode node2 = new RootUINode("Same Window Name", "Same Rotation");
        Assert.assertTrue(node1.equals(node2));
    }

    /**
     * Make two RootUINodes With different window name and same rotation. They
     * should be equal
     *
     * @author georgewpurnell
     */
    @Test
    public void TestRootUINodeEquality4() {
        RootUINode node1 = new RootUINode("Window Name", "Same Rotation");
        RootUINode node2 = new RootUINode("Different Window Name", "Same Rotation");
        Assert.assertFalse(node1.equals(node2));
    }

    /**
     * Make two RootUINodes with the same window name and different rotation.
     * They should be not be equal.
     *
     * @author georgewpurnell
     */
    @Test
    public void TestRootUINodeEquality5() {
        RootUINode node1 = new RootUINode("Same Window Name", "Rotation");
        RootUINode node2 = new RootUINode("Same Window Name", "Different Rotation");
        Assert.assertFalse(node1.equals(node2));
    }

    /**
     * Make two RootUINodes With different window name and different rotation.
     * They should not be equal.
     *
     * @author georgewpurnell
     */
    @Test
    public void TestRootUINodeEquality6() {
        RootUINode node1 = new RootUINode("Window Name", "Rotation");
        RootUINode node2 = new RootUINode("Rotation", "Widow Name");
        Assert.assertFalse(node1.equals(node2));
    }
}
